<?php

namespace Tests\Unit;

//use PHPUnit\Framework\TestCase;
use Tests\TestCase;
use App\Models\Loan;
use App\Models\LoanRepayment;
use App\Models\User;

class UserTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_example()
    {
		$this->assertCount(2, User::all());
		$this->assertCount(2, Loan::all());
		$this->assertCount(3, LoanRepayment::all());
		$this->assertEquals(2, Loan::first()->user_id);
		$this->assertNotNull(User::first()->created_at);
    }
}
