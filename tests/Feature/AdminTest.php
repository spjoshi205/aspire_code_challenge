<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdminTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
		// admin login
		$responseAdmin = $this->json('post', '/api/login', [
											'email' => 'test@gmail.com',
											'password' => 'Admin@123']);
        $responseAdmin
            ->assertStatus(200)
            ->assertJson([
                'sucess' => true,
            ]);
            
        // admin get pending loans
        $responseGetPendingLoans = $this
						->json('get', '/api/pending-loans',[], ['Authorization' => 'Bearer '.$responseAdmin->original['token']]);
        
        $responseGetPendingLoans
            ->assertStatus(200)
            ->assertJson([
                'status' => true
            ]);
        
        // admin approve loan
        $responseApproveLoan = $this->
						withHeaders(['Authorization' => 'Bearer '.$responseAdmin->original['token']])
						->json('post', '/api/approve-loan',[
										"id" => $responseGetPendingLoans->original['results']->last()->id
									]);
        $responseApproveLoan
            ->assertStatus(200)
            ->assertJson([
                'status' => true,
				'message' => 'Loan approved'
            ]);
         
        // admin get all loans
        $responseGetPendingLoans = $this
						->json('get', '/api/loans',[], ['Authorization' => 'Bearer '.$responseAdmin->original['token']]);
        
        $responseGetPendingLoans
            ->assertStatus(200)
            ->assertJson([
                'status' => true
            ]);
    }
}
//php artisan passport:install
