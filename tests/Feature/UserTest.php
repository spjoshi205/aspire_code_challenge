<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        // user login
        $responseUser = $this->json('post', '/api/login', [
											'email' => 'test1@gmail.com',
											'password' => 'Admin@123']);
        $responseUser
            ->assertStatus(200)
            ->assertJson([
                'sucess' => true,
            ]);
            
        // get current user details with user login token
        $responseGetUser = $this
						->json('get', '/api/get-user',[], ['Authorization' => 'Bearer '.$responseUser->original['token']]);
        $responseGetUser
            ->assertStatus(200)
            ->assertJson([
                'sucess' => true,
                'user' => [
					"name" => "test1",
					"email" => "test1@gmail.com"
                ]
            ]);
        
        // user apply for loan
        $responseApplyLoan = $this
						->json('post', '/api/apply-loan',[
										"name" => "Loan",
										"amount" => "10",
										"terms" => "3",
										"repayment_cycle" => "2"
									], ['Authorization' => 'Bearer '.$responseUser->original['token']]);
        $responseApplyLoan
            ->assertStatus(200)
            ->assertJson([
                'status' => true, 
				'message' => "Requst send successfully"
            ]);
        
        // user repayment for loan
        $responseUserLoans = $this
						->json('get', '/api/get-user-loans',[],
						['Authorization' => 'Bearer '.$responseUser->original['token']]);
        $responseUserLoans
            ->assertStatus(200)
            ->assertJson([
                'status' => true
            ]);
        $firstLoanId = '';
        foreach($responseUserLoans->original['results'] as $result){
			if($result->status == 1){
				$firstLoanId = $result->id;
			}
		}
        // user pending repayment for loan
        $responseLoanRepayments = $this
						->json('get', '/api/get-loan-repayments',
						["loan_id" => $firstLoanId],
						['Authorization' => 'Bearer '.$responseUser->original['token']]);
        $responseLoanRepayments
            ->assertStatus(200)
            ->assertJson([
                'status' => true
            ]);
        
        // user pending repayment for loan
        $responsePendingRepayments = $this
						->json('get', '/api/get-pending-repayments',
						["loan_id" => $firstLoanId],
						['Authorization' => 'Bearer '.$responseUser->original['token']]);
        $responsePendingRepayments
            ->assertStatus(200)
            ->assertJson([
                'status' => true
            ]);
        
        // user repayment
        $responseApproveLoan = $this->
						withHeaders(['Authorization' => 'Bearer '.$responseUser->original['token']])
						->json('post', '/api/repayment',[
										"id" => $responsePendingRepayments->original['results']->first()->id,
										"amount" => 5
									]);
        $responseApproveLoan
            ->assertStatus(200)
            ->assertJson([
                'status' => true,
				'message' => 'Repayment successfully'
            ]);
    }
}
