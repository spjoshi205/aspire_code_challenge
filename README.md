# aspire_code_challenge



## Clone project

git clone origin https://gitlab.com/spjoshi205/aspire_code_challenge.git aspire_code_challenge
cd aspire_code_challenge
```

## Install composer 

composer install

## Set database details in env file

## run below command to run migration and seeder
php artisan migrate && php artisan passport:install && php artisan db:seed

## run below command for unit test
php artisan test

