<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Loan;
class LoanRepayment extends Model
{
    use HasFactory;
    protected $table = 'loan_repayment';
    
    /**
     * Get the post that owns the comment.
     */
    public function loan()
    {
        return $this->belongsTo(Loan::class, 'loan_id', 'id');
    }
}
