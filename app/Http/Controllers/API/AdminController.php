<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Loan;
use App\Models\LoanRepayment;
use Carbon\Carbon;

class AdminController extends Controller
{
	
    public function pendingLoans()
    {
        $loans = Loan::where('status', '0')->get();
		return response()->json([
				"status" => true,
				'results' => $loans
				], 200);
    }
	
    public function allLoans()
    {
        $loans = Loan::get();
		return response()->json([
				"status" => true,
				'results' => $loans
				], 200);
    }
	
    public function approveLoan(Request $request)
    {
        $loan = Loan::find($request->id);
        if($loan->status == 0){
			$loan->status = '1';
			$loan->updated_at = Carbon::now();
			$loan->save();
			if($loan){
				//dd($loan);
				$loan_amount = $loan->amount;
				$loan_terms = $loan->terms;
				$emi = round($loan->amount/$loan->terms,2);
				$loan_repayments = [];
				for($i = 1; $i <= $loan_terms; $i++){
					$emi_amount = ($i == $loan_terms) ? $loan_amount : $emi;
					$loan_amount = $loan_amount - $emi;
					if($loan->repayment_cycle == 0){
						$emi_date = Carbon::now()->addDays($i);
					} elseif($loan->repayment_cycle == 1) {
						$emi_date = Carbon::now()->addWeeks($i);
					} elseif($loan->repayment_cycle == 2) {
						$emi_date = Carbon::now()->addMonths($i*2);
					} elseif($loan->repayment_cycle == 3) {
						$emi_date = Carbon::now()->addMonths($i);
					}
					
					$loan_repayments[] = [
									"loan_id" => $request->id,
									"emi_amount" => $emi_amount,
									"intrest_percentage" => 0,
									"emi_date" => $emi_date,
									"status" => '0',
									'created_at' => Carbon::now(),
									'updated_at' => Carbon::now()
									];
				}
				LoanRepayment::insert($loan_repayments);
				return response()->json([
						"status" => true,
						'message' => 'Loan approved'
						], 200);
			} else {
				return response()->json([
						"status" => false,
						'results' => 'Try later'
						], 200);
			}
		} else {
			return response()->json([
						"status" => false,
						'message' => 'Loan already approved'
						], 200);
		}
    }
}
