<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Loan;
use App\Models\LoanRepayment;
use Carbon\Carbon;

class UserController extends Controller
{
	
    public function applyForLoan(Request $request)
    {
		$requestData = $request->all();
		$validator = Validator::make($requestData, [
            'name' => 'required',
            'amount' => 'required',
            'terms' => 'required',
            'repayment_cycle' => 'required',
        ]);
		$requestData['status'] = '0';
		$requestData['outstanding_amount'] = $requestData['amount'];
		$requestData['intrest_percentage'] = 0;
		$requestData['user_id'] = $request->user()->id;
		$requestData['description'] = (isset($requestData['description'])) ? $requestData['description'] : '';
        if ($validator->fails()) {
            return response()->json([
							"status" => false, 
							"message" => $validator->errors()->toArray()
							],401);
        }
        $loan = Loan::create([
			'name' => $requestData['name'],
			'amount' => $requestData['amount'],
			'terms' => $requestData['terms'],
			'repayment_cycle' => $requestData['repayment_cycle'],
			'status' => $requestData['status'],
			'outstanding_amount' => $requestData['outstanding_amount'],
			'intrest_percentage' => $requestData['intrest_percentage'],
			'user_id' => $requestData['user_id'],
			'description' => $requestData['description'],
            'created_at' => Carbon::now(),
			'updated_at' => Carbon::now()
			]);
		if($loan){
			return response()->json([
					"status" => true, 
					'message' => "Requst send successfully"
					], 200);
		} else {
			return response()->json([
					"status" => false,
					'message' => "Try again later"
					], 200);
		}
    }
	
    public function loanRepayment(Request $request)
    {
		$loanRepayment = LoanRepayment::find($request->id);
		if(!empty($loanRepayment) && $loanRepayment->status == 0){
			$loan = Loan::find($loanRepayment->loan_id);
			$validator = Validator::make($request->all(), [
				'amount' => 'required|numeric|min:'.$loanRepayment->emi_amount.'|max:'.$loan->outstanding_amount,
			]);
			if ($validator->fails()) {
				return response()->json([
								"status" => false, 
								"message" => $validator->errors()->toArray()
								],401);
			}
			$pay_amount = $request->amount;
			$loanRepayment->status = '1';
			$loanRepayment->paid_amount = $request->amount;
			$loanRepayment->paid_date = Carbon::now();
			$loanRepayment->updated_at = Carbon::now();
			$loanRepayment->save();
			$outstanding_amount = $loan->outstanding_amount-$request->amount;
			$loan->outstanding_amount = $outstanding_amount;
			if($outstanding_amount <= 0){
				$loan->status = '2';
			}
			$loan->updated_at = Carbon::now();
			$loan->save();
			if($pay_amount > $loanRepayment->emi_amount){
				$limit = ceil($request->amount/$loanRepayment->emi_amount)-1;
				$loanrepaymentList = LoanRepayment::where('loan_id', $loanRepayment->loan_id)
				->where('status', '0')
				->orderBy('id', 'desc')
				->take($limit)
				->get();
				foreach($loanrepaymentList as $loanrepaymentDetail){
					$pay_amount = $pay_amount-$loanrepaymentDetail->emi_amount;
					if($pay_amount >= $loanRepayment->emi_amount){
						$loanrepaymentDetail->delete();
					} else {
						$loanrepaymentDetail->emi_amount = $loanrepaymentDetail->emi_amount-$pay_amount;
						$loanrepaymentDetail->updated_at = Carbon::now();
						$loanrepaymentDetail->save();
					}
				}
			}
			return response()->json([
					"status" => true, 
					'message' => "Repayment successfully"
					], 200);
		} else {
			return response()->json([
					"status" => false,
					'message' => "Payment already done"
					], 200);
		}
    }
	
    public function userLoanList(Request $request)
    {
		$loans = Loan::where('user_id', $request->user()->id)->get();
		return response()->json([
				"status" => true, 
				'results' => $loans
				], 200);
    }
	
    public function userRepaymentList(Request $request)
    {
		$loanRepayment = LoanRepayment::where('loan_id', $request->loan_id)->get();
		return response()->json([
				"status" => true, 
				'results' => $loanRepayment
				], 200);
    }
	
    public function userPendingRepaymentList(Request $request)
    {
		$loanRepayment = LoanRepayment::where('loan_id', $request->loan_id)->where('status', '0')->get();
		return response()->json([
				"status" => true, 
				'results' => $loanRepayment
				], 200);
    }
}
