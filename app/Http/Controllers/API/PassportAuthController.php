<?php
 
namespace App\Http\Controllers\API;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class PassportAuthController extends Controller
{
    /**
     * Registration Req
     */
    protected $tokenName = 'user-login-token';
    public function register(Request $request)
    {
		$requestData = $request->all();
		$validator = Validator::make($requestData, [
            'name' => 'required|min:4',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8'
        ]);
		$requestData['role'] = 'user';
        if ($validator->fails()) {
            return response()->json(["status" => false, "message" => $validator->errors()->toArray()],401);
        }
        $user = User::create([
							'name' => $requestData['name'],
							'email' => $requestData['email'],
							'password' => $requestData['password'],
							'role' => 'user',
							'created_at' => Carbon::now(),
							'updated_at' => Carbon::now()
							]);
		$token = $user->createToken($this->tokenName)->accessToken;
  
        return response()->json(["status" => true, 'token' => $token], 200);
    }
  
    /**
     * Login Req
     */
    public function login(Request $request)
    {
        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];
  
        if (auth()->attempt($data)) {
            $token = auth()->user()->createToken($this->tokenName)->accessToken;
            return response()->json(['sucess' => true, 'token' => $token], 200);
        } else {
            return response()->json(['sucess' => true, 'error' => 'Unauthorised'], 401);
        }
    }
    
	/**
     * Login user info
     */
    public function userInfo() 
    {
 
     $user = auth()->user();
      
     return response()->json(['sucess' => true, 'user' => $user], 200);
 
    }
    
	/**
     * Login user info
     */
    public function usersList() 
    {
 
     $users = User::get();
      
     return response()->json(['user' => $users], 200);
 
    }
}
