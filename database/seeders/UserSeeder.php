<?php
 
namespace Database\Seeders;
 
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Loan;
use App\Models\LoanRepayment;
use Carbon\Carbon;
 
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
		// create admin user
        User::create([
            'name' => 'test',
            'email' => 'test@gmail.com',
            'password' => 'Admin@123',
            'role' => 'admin',
            'created_at' => Carbon::now(),
			'updated_at' => Carbon::now()
        ]);
        // create notmal user
        User::create([
            'name' => 'test1',
            'email' => 'test1@gmail.com',
            'password' => 'Admin@123',
            'role' => 'user',
            'created_at' => Carbon::now(),
			'updated_at' => Carbon::now()
        ]);
        // create loan without approve
        Loan::create([
			'name' => 'Personal Loan',
			'amount' => '10',
			'terms' => '3',
			'repayment_cycle' => '2',
			'status' => '0',
			'outstanding_amount' => '10',
			'intrest_percentage' => '0',
			'user_id' => '2',
			'description' => '',
            'created_at' => Carbon::now(),
			'updated_at' => Carbon::now()
			]);
		// create approved loan
        $laon = Loan::create([
			'name' => 'Personal Loan',
			'amount' => '10',
			'terms' => '3',
			'repayment_cycle' => '2',
			'status' => '1',
			'outstanding_amount' => '10',
			'intrest_percentage' => '0',
			'user_id' => '2',
			'description' => '',
            'created_at' => Carbon::now(),
			'updated_at' => Carbon::now()
			]);
		$loan_repayments = [
							[
								"loan_id" => $laon->id,
								"emi_amount" => '3.33',
								"intrest_percentage" => 0,
								"emi_date" => Carbon::now(),
								"status" => '0',
								'created_at' => Carbon::now(),
								'updated_at' => Carbon::now()
							],
							[
								"loan_id" => $laon->id,
								"emi_amount" => '3.33',
								"intrest_percentage" => 0,
								"emi_date" => Carbon::now()->addWeeks(1),
								"status" => '0',
								'created_at' => Carbon::now(),
								'updated_at' => Carbon::now()
							],
							[
								"loan_id" => $laon->id,
								"emi_amount" => '3.34',
								"intrest_percentage" => 0,
								"emi_date" => Carbon::now()->addWeeks(2),
								"status" => '0',
								'created_at' => Carbon::now(),
								'updated_at' => Carbon::now()
							]
						];
		// insert repayment data 
		LoanRepayment::insert($loan_repayments);
    }
}
