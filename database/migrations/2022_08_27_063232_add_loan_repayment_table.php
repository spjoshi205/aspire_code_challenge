<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLoanRepaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_repayment', function (Blueprint $table) {
            $table->id();
            $table->foreignId('loan_id');
            $table->float('emi_amount', 8, 2);
            $table->float('paid_amount', 8, 2);
            $table->float('intrest_percentage', 8, 2);
            $table->dateTime('emi_date');
            $table->dateTime('paid_date');
            $table->enum('status', [0, 1]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('loan_repayment');
    }
}
