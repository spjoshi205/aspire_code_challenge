<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLoanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->float('amount', 8, 2);
            $table->foreignId('user_id');
            $table->float('intrest_percentage', 8, 2);
            $table->float('outstanding_amount', 8, 2);
            $table->integer('terms');
            $table->enum('status', [0, 1, 2]);
            $table->enum('repayment_cycle', [0, 1, 2, 3]);
            $table->longText('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('loan');
    }
}
