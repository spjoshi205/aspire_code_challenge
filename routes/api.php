<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\PassportAuthController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\AdminController;
 
Route::post('register', [PassportAuthController::class, 'register']);
Route::post('login', [PassportAuthController::class, 'login']);
Route::middleware('auth:api')->group(function () {
	Route::middleware(['roles:user'])->group(function () {
		Route::get('get-user', [PassportAuthController::class, 'userInfo']);
		Route::post('apply-loan', [UserController::class, 'applyForLoan']);
		Route::get('get-user-loans', [UserController::class, 'userLoanList']);
		Route::get('get-loan-repayments', [UserController::class, 'userRepaymentList']);
		Route::get('get-pending-repayments', [UserController::class, 'userPendingRepaymentList']);
		Route::post('repayment', [UserController::class, 'loanRepayment']);
    });
    Route::middleware(['roles:admin'])->group(function () {
		Route::get('get-users', [PassportAuthController::class, 'usersList']);
		Route::get('pending-loans', [AdminController::class, 'pendingLoans']);
		Route::post('approve-loan', [AdminController::class, 'approveLoan']);
		Route::get('loans', [AdminController::class, 'allLoans']);
    });
});

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
*/
